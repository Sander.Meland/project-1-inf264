import copy

class Node():

    '''
    This class will be used for making a decision tree whith the best possible nodes. 
    The children nodes are referred to as left and right. 
    The split value is supposed to be the mean of the feature index values. 
    The value is used when the node is a leaf.
    '''
    def __init__(self, split_value = None, feature_index = None, left = None, right = None, label:str =None, majority_label = None):
        self.split_value = split_value
        self.feature_index = feature_index
        self.left = left
        self.right = right
        self.label = label
        self.majority_label = majority_label
        
    def set_left(self, node):
        self.left = node
        
    def set_right(self, node):
        self.right = node

    def set_split_value(self, val:float):
        self.split_value = val
    
    def set_feature_index(self, index:float):
        self.feature_index = index

    def remove_node(self):
        self.left==None
        self.right ==None
    
    def is_leaf(self):
        if self.left==None and self.right==None and self.feature_index == None \
             and self.split_value==None and self.label != None:
             return True

    def set_label(self, label):
        self.label = label

    def get_label(self):
        return self.label
   
    def replace_with_majority_label(self, label):
        self.left = None
        self.right = None
        self.label = label
        self.feature_index = None
        self.split_value = None

    def copy(self):
        return Node(split_value = self.split_value, feature_index = self.feature_index, left = self.left, right = self.right, label =self.label, majority_label= self.majority_label)

   
    
    def get_majority_label(self) -> str:
        return self.majority_label
    





    
    

    


    

