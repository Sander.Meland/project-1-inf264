import numpy as np
import pandas as pd
from sklearn import model_selection
import time
from Node import Node


def probability_for_features(index, X):
    '''
    Returns the probability for the feature values to be below and over the mean of the given feature values
    '''
    index_string = str(index)

    mean_value = X[index_string].mean()

    n_below_mean= []
    n_over_mean = []

    for int in X[index_string]:
        if int >= mean_value:
            n_over_mean.append(int)
        else:
            n_below_mean.append(int)

    if len(X[index_string]) <= 0:
        probability_over = 0
        probability_under = 0
    else:
        probability_over = len(n_over_mean)/len(X[index_string])
        probability_under = len(n_below_mean)/len(X[index_string])

    return probability_under, probability_over
    
def split_list(index, X, Y): 
    '''
    Vil splitte for å sortere to lister hvor den ene kun har verdier i den ene featuren som er over mean og den andre under. 
    Kan da bruke denne i conditional entropy
    '''
    index_string = str(index)

    mean_value = X[index_string].mean()

    split_train = X.copy()

    split_train.insert(len(X.columns), "label", Y)

    over_mean = split_train.loc[split_train[index_string] >= mean_value]
    below_mean = split_train.loc[split_train[index_string] < mean_value]
    
    return below_mean, over_mean, mean_value

def probability_for_label(Y):
    '''
    check the the probability for labels
    '''
    Ylist = Y.tolist()
    n_g_labels = Ylist.count('g')
    n_h_labels = Ylist.count('h')

    if len(Ylist) <=0:
        probability_g =0
        probability_h = 0
    else:
        probability_g = n_g_labels/len(Y)
        probability_h = n_h_labels/len(Y)

    return probability_g, probability_h

def get_max_probability_for_label(Y):
    probability_g, probability_h = probability_for_label(Y)
    if probability_g > probability_h:
        return 'g'
    elif probability_h > probability_g:
        return 'h'
    else:
        return "same"

def gini_values_for_features(X, Y):
    gini_list = []
    for i in range(10):
        below_mean, over_mean, mean_value = split_list(i, X, Y)
        probability_g_under, probability_h_under = probability_for_label(below_mean["label"])
        probability_g_over, probability_h_over = probability_for_label(over_mean["label"])
        gini_val_under = 1- (probability_g_under**2 + probability_h_under**2)
        gini_val_over = 1 - (probability_g_over**2 + probability_h_over**2)
        gini_val_feature = gini_val_over + gini_val_under
        gini_list.append(gini_val_feature)
    return gini_list

def entropy(Y):
    '''
    Calculates entropy for two probabilities where index 0 has lower than average, and index 1 has higher
    
    '''
    probability_g, probability_h = probability_for_label(Y)

    if probability_g<=0 or probability_h<= 0:
            entropy_val=0
    else:
        entropy_val = -(probability_g*np.log2(probability_g)+probability_h*np.log2(probability_h))
    return entropy_val
    
def conditional_entropy(index, X, Y):
    '''
    Amount of information needed to describe the outcome of a random variable y when the value of a random variable x is known
    Or amount of uncertainty of y when the value of x is known
    H(y|x) = sum(P(x=xi)*H(y|x=xi)) = -sum(P(x=xi))*sum(P(y=yj|x=xi)
    '''

    probability_under, probability_over = probability_for_features(index, X)

    below_mean, over_mean, mean_value = split_list(index, X, Y)



    #H_lab_under, H_lab_over = given_probability(index, X, Y)

    return probability_under*entropy(below_mean.iloc[:, -1]) + probability_over*entropy(over_mean.iloc[:, -1])
    
def best_information_gain(X, Y, impurity_measure):
    '''
    Function to find the feature with max information gain. Returns the index of the feature with max information gain
    Takes the impurity measure as input whereas it corresponds to the learn function
    '''

    if impurity_measure =="entropy":
        ig_list= []
        entropy_val = entropy(Y)
        for i in range(10):
            cond_entropy_val = conditional_entropy(i, X, Y)
            ig_val = entropy_val-cond_entropy_val
            ig_list.append(ig_val)
        return ig_list.index(max(ig_list))
    else:
        gini_list = gini_values_for_features(X, Y)
        return gini_list.index(min(gini_list))
    
def check_all_equal(list):
    return len(set(list)) == 1

def check_all_feature_values(X):
    check_list = [True]
    for i in range(10):
        check_list.append(check_all_equal(X[str(i)]))
    if check_all_equal(check_list):
        return True
   
    return False
            
def _learn(X, Y, impurity_measure="gini", pruning = False): 
    '''
    Function to learn a decision classifier from a data matrix X and a label vector Y.
    Impurity measure is chosen when running the function, the input should be either one of entropy or gini

    '''
    if check_all_equal(Y) == True:
        return Node(label = Y.iloc[0]) #leaf
        
    elif check_all_feature_values(X) == True:
        probability_g, probability_h = probability_for_label(Y)
        if probability_g > probability_h:
            return Node(label= 'g')
        else:
            return Node(label = 'h')

    else:
        best_index = best_information_gain(X, Y, impurity_measure)
        below_mean, over_mean, mean_value = split_list(best_index, X, Y)
        majority_label= get_max_probability_for_label(Y)
        node = Node(split_value= mean_value, feature_index= best_index, majority_label=majority_label)
        node_below = _learn(below_mean.iloc[:,[0,1,2,3,4,5,6,7,8,9]], below_mean.iloc[:,-1], impurity_measure)
        node_over = _learn(over_mean.iloc[:, [0,1,2,3,4,5,6,7,8,9]], over_mean.iloc[:,-1], impurity_measure)
        node.set_left(node_below)
        node.set_right(node_over)
        return node

def learn(X, Y, impurity_measure = "gini", pruning = False) -> Node:
    if pruning == True:
        X_train, X_prun, Y_train, Y_prun = model_selection.train_test_split(
        X, Y, test_size=0.3, shuffle= True, random_state=1337)
        root = _learn(X_train, Y_train, impurity_measure)
        prune(X_prun, Y_prun, root)
        return root
        
        
def predict(x, tree:Node):
    '''
    Predicts the class label of a new datapoint x which is all the feature values for each row
    '''
    if tree.is_leaf(): 
        return tree.get_label()
    if x[tree.feature_index] >= tree.split_value:
        node = predict(x, tree.right)
    else:
        node = predict(x, tree.left)
    return node
    
def score_for_tree(X, Y, tree:Node):
    '''
    Finds the accuracy score of the tree
    '''
    accuracy_train = 0
    
    
    for row in range(len(X.to_numpy())):
        prediction = predict(X.to_numpy()[row], tree)
        if Y.to_numpy()[row] == prediction:
            accuracy_train +=1
    if len(Y.to_numpy())>0:
        accuracy_score = accuracy_train/len(Y.to_numpy())
    else:
        accuracy_score = 0

    return accuracy_score

def prune(X_prune, Y_prune, node:Node)->Node:
    '''
    Function to tackle overfitting using reduced-error pruning
    X, Y = X_prune, Y_prune
    Bør kanskje bruke split metoden, kanskje ikke - splitte train datasettet
    Bør også ta inn X_train og Y_train -> Holder det at disse ligger i node?
    X_prune og Y_prune skal bare brukes til å regne ut accuracy
    node = rooten som er lagd med training data
    '''
    if node.is_leaf():
        return

    prune(X_prune[X_prune.iloc[:, node.feature_index] < node.split_value], Y_prune[X_prune.iloc[:, node.feature_index] < node.split_value], node.left)
   
    prune(X_prune[X_prune.iloc[:, node.feature_index] >= node.split_value], Y_prune[X_prune.iloc[:, node.feature_index] >= node.split_value], node.right)
    
    new_node = node.copy()
    new_node.replace_with_majority_label(label = node.get_majority_label()) #endrer kopien som er et subtree, til et leaf med label= majority label til den opprinnelige noden
    
    if score_for_tree(X_prune, Y_prune, new_node) >= score_for_tree(X_prune, Y_prune, node): # ser ut som det blir samme verdi hver gang
        node.replace_with_majority_label(node.get_majority_label()) #pruner hver gang, så må sjekke denne
       
def main():
    cols = ["0","1", "2", "3", "4", "5", "6", "7", "8", "9", "label"]
    
    data = pd.read_csv("magic04.data", ",", header = None)
    data.columns = cols
    
    X = data.iloc[:, [0,1,2,3,4,5,6,7,8,9]]
    Y = data.iloc[:,-1]

    seed = 1337                  # Fix random seed for reproducibility
    # Shuffle and split the data into train and a 
    # concatenation of validation and test sets with a ratio of 0.7/0.3:
    X_train_prun, X_val_test, Y_train_prun, Y_val_test = model_selection.train_test_split(
        X, Y, 
        test_size=0.3, shuffle=True, random_state=seed)

    # Shuffle and split the data into validation and test sets with a ratio of 0.5/0.5:
    X_val, X_test, Y_val, Y_test = model_selection.train_test_split(
        X_val_test, Y_val_test, 
        test_size=0.5, shuffle=True, random_state=seed) 

    #make the trees
    #start_time = time.time()
    gini_tree = _learn(X_train_prun, Y_train_prun, "gini")
    entropy_tree = _learn(X_train_prun, Y_train_prun, "entropy")
    gini_tree_pruning = learn(X_train_prun, Y_train_prun, "gini", pruning =True)
    entropy_tree_pruning = learn(X_train_prun, Y_train_prun, "entropy", pruning = True)
    #Collecting accuracies for the different trees
    gini_score = score_for_tree(X_val, Y_val, gini_tree)
    entropy_score = score_for_tree(X_val, Y_val, entropy_tree)
    gini_score_pruning = score_for_tree(X_val,Y_val, gini_tree_pruning)
    entropy_score_pruning = score_for_tree(X_val, Y_val, entropy_tree_pruning)

    print(gini_score)
    print(entropy_score)
    print(gini_score_pruning)
    print(entropy_score_pruning)
    
    '''
    #creating a dataframe to represent the results
    data = [["Gini w/o pruning", gini_score], ["Entropy w/o pruning", entropy_score], ["Gini with pruning", gini_score_pruning], ["Entropy with prunig", entropy_score_pruning]]
    score_df = pd.DataFrame(data, columns = ["Test", "Score"])
    print("\n Runtime : --- %s s--- " %(time.time()-start_time))
    print("Validation accuracies on the different trees: \n" + score_df)

    #evaluate the model on test data
    best_score = score_df.max()
    print("The best model tested on the testing data gives this accuracy: %.5f" % score_for_tree(X_test, Y_test, entropy_tree_pruning))
'''

    
    


    
if __name__ == '__main__':
    main()
    

    
        
        





        


